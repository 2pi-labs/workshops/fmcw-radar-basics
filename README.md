[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/2pi-labs%2Fworkshops%2Ffmcw-radar-basics/HEAD)

# FMCW Radar Basics Workshop
Here you can find our basic workshop for FMCW radar signal processing. You can start with looking around in the notebooks subfolder or run this workshop interactively using [mybinder](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.com%2F2pi-labs%2Fworkshops%2Ffmcw-radar-basics/HEAD) (Note: This can sometimes take a while to load).

